<?php

define('CROSSCLONE_CLONE_STRAIGHT', 1);

define('CROSSCLONE_CLONE_SIMILAR', 2);

define('CROSSCLONE_COUPLE_NONE', 0);

define('CROSSCLONE_COUPLE_DOWNSTREAM_STRAIGHT', 1);

define('CROSSCLONE_COUPLE_DOWNSTREAM_SIMILAR', 2);

define('CROSSCLONE_COUPLE_UPSTREAM_STRAIGHT', 3);

define('CROSSCLONE_COUPLE_UPSTREAM_SIMILAR', 4);

define('CROSSCLONE_COUPLE_CROSS_STRAIGHT', 5);

define('CROSSCLONE_COUPLE_CROSS_SIMILAR', 6);

define('CROSSCLONE_DELETE_NONE', 0);

define('CROSSCLONE_DELETE_DOWNSTREAM', 1);

define('CROSSCLONE_DELETE_UPSTREAM', 2);

define('CROSSCLONE_DELETE_CROSS', 3);

/**
 * Central clone functionality for nodes.
 */
function crossclone_node($original_node, $settings, $save = FALSE) {
  if (!isset($settings['type'])) {
    return FALSE;
  }

  if (is_numeric($original_node)) {
    $original_node = node_load($original_node);
  }

  // Set defaults.
  $default_settings = array(
    'clone' => CROSSCLONE_CLONE_SIMILAR,
    'couple' => CROSSCLONE_COUPLE_CROSS_SIMILAR,
    'delete' => CROSSCLONE_DELETE_DOWNSTREAM,
  );
  $settings = array_merge($default_settings, $settings);

  // Only clone if we found an original node.
  if (isset($original_node->nid)) {
    $node = drupal_clone($original_node);

    // Prepare some necessary properties.
    $node->crossclone_original = $original_node->nid;
    $node->crossclone_couple = $settings['couple'];
    $node->crossclone_delete = $settings['delete'];
    $node->type = $settings['type'];
    $node->nid = NULL;
    $node->vid = NULL;
    $node->tnid = NULL;
    $node->created = NULL;
    $node->path = NULL;

    // Decide if we shall clone similar fields.
    if ($settings['clone'] == CROSSCLONE_CLONE_SIMILAR) {
      $info = content_types($original_node->type);
      foreach ($info['fields'] as $field) {
        $similar_field_name = strtr($field['field_name'], array($original_node->type => $settings['type']));
        $node->{$similar_field_name} = $original_node->{$field['field_name']};
      }
    }

    // Let other modules modify the clone before save.
    drupal_alter('crossclone_node', $node, $original_node);

    if ($save) {
      return node_save($node);
    }
    return $node;
  }
  return FALSE;
}

/**
 * Implementation of hook_nodeapi().
 */
function crossclone_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'load':
      $result = db_query('SELECT * FROM {crossclone} WHERE nid = %d', $node->nid);
      while ($row = db_fetch_object($result)) {
        $node->crossclone_original = $row->crossclone_original;
        $node->crossclone_couple = $row->crossclone_couple;
        $node->crossclone_delete = $row->crossclone_delete;
      }
      break;

    case 'insert':
      if (!empty($node->crossclone_original)) {
        drupal_write_record('crossclone', $node);
      }
      break;

    case 'update':
      if (!empty($node->crossclone_original)) {
        drupal_write_record('crossclone', $node, array('nid'));
      }
      break;

    case 'delete':
      // TODO
      break;
  }
}

/**
 * Implementation of hook_token_list().
 */
function crossclone_token_list($type = 'all') {
  $tokens = array();
  if ($type == 'node' || $type == 'all') {
    $tokens['node']['crossclone-original'] = t('Node ID from which this node was cloned.');
  }
  return $tokens;
}

/**
 * Implementation of hook_token_values().
 */
function crossclone_token_values($type, $object = NULL) {
  $values = array();
  if ($type == 'node') {
    $values['crossclone-original'] = isset($node->crossclone_original) ? $node->crossclone_original : 0;
  }
  return $values;
}

/**
 * Helper function to fetch clone options.
 */
function crossclone_get_clone_options($node_type = NULL) {
  return array(
    CROSSCLONE_CLONE_STRAIGHT => t('Clone straight'),
    CROSSCLONE_CLONE_SIMILAR => t('Clone similar'),
  );
}

/**
 * Helper function to fetch couple options.
 */
function crossclone_get_couple_options($node_type = NULL) {
  return array(
    CROSSCLONE_COUPLE_NONE => t('No coupling'),
    CROSSCLONE_COUPLE_DOWNSTREAM_STRAIGHT => t('Couple downstream, straight'),
    CROSSCLONE_COUPLE_DOWNSTREAM_SIMILAR => t('Couple downstream, similar'),
    CROSSCLONE_COUPLE_UPSTREAM_STRAIGHT => t('Couple upstream, straight'),
    CROSSCLONE_COUPLE_UPSTREAM_SIMILAR => t('Couple upstream, similar'),
    CROSSCLONE_COUPLE_CROSS_STRAIGHT => t('Cross couple, straight'),
    CROSSCLONE_COUPLE_CROSS_SIMILAR => t('Cross couple, similar'),
  );
}

/**
 * Helper function to fetch delete options.
 */
function crossclone_get_delete_options($node_type = NULL) {
  return array(
    CROSSCLONE_DELETE_NONE => t('No delete coupling'),
    CROSSCLONE_DELETE_DOWNSTREAM => t('Delete downstream'),
    CROSSCLONE_DELETE_UPSTREAM => t('Delete upstream'),
    CROSSCLONE_DELETE_CROSS => t('Cross delete'),
  );
}
