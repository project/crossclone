<?php

/**
 * Implementation of hook_rules_action_info().
 */
function crossclone_rules_action_info() {
  return array(
    'crossclone_node_action' => array(
      'label' => t('Cross clone node'),
      'module' => 'Node',
      'arguments' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Content'),
        ),
      ),
    ),
  );
}

/**
 * Action callback.
 */
function crossclone_node_action($original_node, $settings) {
  $node = crossclone_node($original_node, $settings);
  return array('node' => $node);
}

/**
 * Form callback.
 */
function crossclone_node_action_form($settings, &$form) {
  $form['settings']['type'] = array(
    '#type' => 'select',
    '#title' => t('New node type'),
    '#description' => t('Choose which node type the clone shall have.'),
    '#options' => node_get_types('names'),
    '#default_value' => $settings['type'],
  );
  $form['settings']['clone'] = array(
    '#type' => 'select',
    '#title' => t('Clone settings'),
    '#description' => t('Choose how to clone the original node.'),
    '#options' => crossclone_get_clone_options(),
    '#default_value' => !empty($settings['clone']) ? $settings['clone'] : CROSSCLONE_CLONE_SIMILAR,
  );
  $form['settings']['couple'] = array(
    '#type' => 'select',
    '#title' => t('Coupling settings'),
    '#description' => t('Choose how the original node should couple to the clone.'),
    '#options' => crossclone_get_couple_options(),
    '#default_value' => !empty($settings['couple']) ? $settings['couple'] : CROSSCLONE_COUPLE_CROSS_SIMILAR,
  );
  $form['settings']['delete'] = array(
    '#type' => 'select',
    '#title' => t('Delete settings'),
    '#description' => t('Choose how the original node should behave when it gets deleted.'),
    '#options' => crossclone_get_delete_options(),
    '#default_value' => !empty($settings['delete']) ? $settings['delete'] : CROSSCLONE_DELETE_DOWNSTREAM,
  );
}
